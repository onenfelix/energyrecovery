from django.http import HttpResponseRedirect
from django.shortcuts import render
import wntr 
import pandas as pd
from  epanettools.epanettools import EPANetSimulation, Node, Link, Network, Nodes,Links, Patterns, Pattern, Controls, Control

from .forms import DataForm
from .optimiser import handleNetworkFile
from .models import Maximised


def index(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = DataForm(request.POST, request.FILES)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            handleNetworkFile(request.FILES['datafile'],form.cleaned_data['duration'],form.cleaned_data['timestep'])
            # redirect to a new URL:
            data = Maximised.objects.all()
            context = {"data": data}
            return render(request, "tool/results.html", context)

    # if a GET (or any other method) we'll create a blank form
    else:
        form = DataForm()

    return render(request, 'tool/home.html', {'form': form})

def results(request):
    return render(request, "tool/results.html", {})

def energy(request):
    #open network file
    wn = wntr.network.WaterNetworkModel("networks/network.inp") 
    es=EPANetSimulation("networks/network.inp")
    #link information for the network
    m=es.network.links
    #obtain optimised pipes
    pipes = Maximised.objects.all()
    #loop through all the optimised pipes
    for pipe in pipes:
        startNode = m[pipe.pipe_id].start.id
        endNode = m[pipe.pipe_id].end.id
        name = 'valve_' + pipe.pipe_id
        wn.remove_link(pipe.pipe_id)
        wn.add_valve(name=name, start_node_name=startNode,minor_loss=0, end_node_name=endNode,valve_type='PRV', diameter=158.6)
        
        #declare constants
        sigma = 9810
        d = 5000
        #initialise energy dataframe for capturing information
        #energydataframe = None
        #energydataframe = pd.DataFrame(columns = ['Energy','headloss','discharge','pipe','PRV','maximised'], index=[]) 

        #obtain the maximum energy recovery

        #run simulation inorder to obtain results to compute the required values
        sim = wntr.sim.EpanetSimulator(wn)
        results = sim.run_sim()
        headlossDataframe = results.link['headloss']
        pressureDataframe = results.node['pressure']
        dischargeDataframe = results.link['flowrate']
        # wn.get_link(name).minor_loss = km
        #initialise an empty dataframe for storing the maximised energy for each km
        Finaldataframe = None
        Finaldataframe = pd.DataFrame(columns = ['Energy','headloss','discharge','pipe'], index=[]) 
        km = 0
        while km < 200000:
            #initialise an empty energy dataframe for each km
            energydataframe = None
            energydataframe = pd.DataFrame(columns = ['Energy','headloss','discharge','pipe'], index=[]) 
            #initialise energy recovered for each km
            energyRecovered = 0
            previoustimestep = 0
            #loop through the discharge dataframe
            for i, j in dischargeDataframe.iterrows():  
                discharge = dischargeDataframe.loc[i,name]
                headloss = km * (8/(pow(3.14,2) * 9.8)) * pow((discharge/158.6), 2)
                node_pressure = pressureDataframe.loc[i,endNode]
                if previoustimestep == 0:
                    energyBefore = 0
                else:
                    energyBefore = energydataframe.loc[previoustimestep,'Energy']
                energyRecovered = (sigma * discharge * headloss * 24) / 1000
                #insert the energy result to the temporary dataframe
                new_row = pd.Series(data={'Energy':energyRecovered, 'headloss':headloss, 'discharge':discharge, 'pipe':name}, name=i)
                energydataframe = energydataframe.append(new_row, ignore_index=False)
                #check if energy recovered is less than the previous
                if (energyRecovered < energyBefore) or (node_pressure < 20):
                    Finaldataframe = Finaldataframe.append(new_row, ignore_index=False)
                    break
                previoustimestep = i

            #insert values to the energy dataframe
            # new_row = pd.Series(data={'Energy':energyRecovered, 'headloss':headloss, 'discharge':discharge, 'pipe':name, 'PRV':km, 'maximised':'false'}, name=km)
            # # print(new_row)
            # energydataframe = energydataframe.append(new_row, ignore_index=False)
            # if (energyRecovered < energyBefore) or (node_pressure < 20):
            #     minimum_discharge = energydataframe['discharge'].min()
            #     mean_discharge = energydataframe['discharge'].mean()
            #     minimum_headloss = energydataframe['headloss'].min()
            #     mean_headloss = energydataframe['headloss'].mean()
            #     energy_value = energydataframe.loc[km,'Energy'].item()
            #     prv = energydataframe.loc[km,'PRV']

                # print(minimum_discharge)
                # print(mean_discharge)
                # print(minimum_headloss)
                # print(mean_headloss)
                # print(energy_value)

                # break
            km = km + d
            # print(discharge = dischargeDataframe.loc[km,valve_2]).item()
            # print(discharge = dischargeDataframe.loc[km,valve_2]).item()
        # print(dischargeDataframe)
        # print(wn.get_link(pipe.pipe_id).flow)
        # print(dischargeDataframe.loc[:,name])
        print(Finaldataframe)







    data = pipes
    return render(request, 'tool/results.html', {'data': data})

