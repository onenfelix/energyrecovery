from django.db import models

class Maximised(models.Model):
    scenario = models.IntegerField()
    pipe_id = models.CharField(max_length=250)
    headloss = models.DecimalField(max_digits=8, decimal_places=4)
    discharge = models.DecimalField(max_digits=8, decimal_places=4)
    hydraulic_power = models.DecimalField(max_digits=8, decimal_places=4)

    def __str__(self):
        return self.pipe_id
