import wntr 
import pandas as pd
from  epanettools.epanettools import EPANetSimulation, Node, Link, Network, Nodes,Links, Patterns, Pattern, Controls, Control
from .models import Maximised

def handleNetworkFile(f,d,t):
    # f = f.close()
    with open('networks/network.inp', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    #open network file
    wn = wntr.network.WaterNetworkModel("networks/network.inp") 
    es=EPANetSimulation("networks/network.inp")

    #delete simulation previous optimisation result from the database
    Maximised.objects.all().delete()
    #set the simulation duration and timestep
    wn.options.time.duration = d
    wn.options.time.hydraulic_timestep = t
    print(wn.options.time.hydraulic_timestep)
    
    #Run simulation using the above timesteps and duration
    sim = wntr.sim.EpanetSimulator(wn)

    results = sim.run_sim()
    #link information for the network
    m=es.network.links

    #retrieve headloss dataframe from link result
    headlossDataframe = results.link['headloss']
    pressure = results.node['pressure']
    dischargeDataframe = results.link['flowrate']
    # headlossDataframe.info()
    # print(headlossDataframe.idxmax())
    # print(headlossDataframe.max(axis=1))
    # headlossDataframe = results.node
    # print(headlossDataframe)

    #constants
    pressure_minimum = 20

    #maximised pipes dataframe
    maximised_pipes = pd.DataFrame(columns = ['Name'], index=[]) 

    links_on_nodes = wn.get_links_for_node('2',flag='ALL')
    # print(links_on_nodes)
    # print(type(links_on_nodes))

    df = headlossDataframe
    # # # print(headlossDataframe.index)
    # # print(type(df))
    columns = list(df)

    
    #loop through the rows(timesteps) of the headloss dataframe
    for i, j in df.iterrows(): 
        data = []
        index = []
        # print(i, j) 
        # print("row", i)
        
        for r in columns: 
             # printing the third element of the column 
            # print("column", r)
            data.append(df[r][i])
            # print(data.append(df[r][i]))
            index.append(r)
        # print(data)    
        ts = pd.Series(data, index=index)#create a pandas series of headloss as data and pipe id as indicies
        # print(ts)
        sorted = ts.nlargest(n=42, keep='first')#sort the pipe from the max headloss to min
        # print(sorted)

        #determine the maximum link for a timestep
        for ind, value in sorted.items():
            pipe_maximised = False

            #obtain nodes attached to the link
            nodes = []
            nodes.append(m[ind].start.id)
            nodes.append(m[ind].end.id)
            #check if any of the nodes maximises at this timestep, by looping through and checking
            for node in nodes:
                # print("node", node)
                pressure_at_thisnode = pressure.loc[:,node]
                pressure_at_thisnode = pressure_at_thisnode[i]
                # print(pressure_at_thisnode)
                if pressure_at_thisnode > pressure_minimum:
                    # print("greater", node)
                    maximised_pipes.loc[i] = ind
                    pipe_maximised = True
                    break
                else:
                    continue
            if pipe_maximised == True:# jump to the next timestep after obtaining a maximised node for this timestep
                break

        
        
    # print(maximised_pipes)
    # print(df)
    
    # print([m[24].start.id,m[24].end.id])
    # print(pressure.loc[:,'18'])
    # print(maximised_pipes)

    #GENERATE MAXIMISED PIPES UNIQUE BASING ON MAXIMUM HEADLOSS

    #retrieve duplicated pipes from maximised pipes
    duplicated = maximised_pipes.drop_duplicates(subset=['Name'], keep='last')

    temp_maximised_pipes = maximised_pipes.copy() #copy of maximised pipes for future computation
    #create a cleaned dataframe of maximised pipes without duplicate
    for i, j in duplicated.iterrows(): 
        #print(duplicated.loc[i,"Name"])
        maximised_pipes = maximised_pipes[maximised_pipes.Name != duplicated.loc[i,"Name"]]

    """loop through the dataframe of unique duplicated elements to obtain their
    headlosses and determine the one with maximum headloss"""
    
    for i, j in duplicated.iterrows(): 
        pipe_id = duplicated.loc[i,"Name"]
        workframe = temp_maximised_pipes[temp_maximised_pipes.Name == pipe_id].copy()
        workframe['headloss'] = 275.567
        
        for i, j in workframe.iterrows():
            # print(duplicated.loc[i,"Name"])
            workframe.loc[i,"headloss"] = headlossDataframe.loc[i,pipe_id]
        workframe = workframe[workframe.headloss == workframe.headloss.max()]
        workframe = workframe.drop_duplicates(subset=['headloss'], keep='last')
        # print(type(workframe["Name"].first_valid_index()))
        # print(maximised_pipes[workframe["Name"].first_valid_index()])
        maximised_pipes.loc[workframe["Name"].first_valid_index()] = workframe.loc[workframe["Name"].first_valid_index(), 'Name']

    """looping through the maximised pipes and obtaining required data 
    and saving in the database"""
    for i, j in maximised_pipes.iterrows():
        scenario = i
        pipe_id = maximised_pipes.loc[i,"Name"]
        headloss = round(headlossDataframe.loc[i,pipe_id].item(), 3)
        discharge = round(dischargeDataframe.loc[i,pipe_id].item(), 3)
        hydraulic_power = round((9810 * discharge * headloss * 24) / 1000, 3)

        maximised_pipe = Maximised(scenario=scenario,pipe_id=pipe_id,headloss=headloss,discharge=discharge,hydraulic_power=hydraulic_power)
        maximised_pipe.save()
      
        


    # pipes_24 = maximised_pipes.loc[maximised_pipes['Name'] == '24']
    # print(pipes_24)
    # print(type(pipes_24))



    

    

