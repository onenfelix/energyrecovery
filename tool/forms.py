from django import forms

class DataForm(forms.Form):
    datafile = forms.FileField(label='Network File',widget=forms.FileInput(attrs={'class': 'font-weight-bold col-md-3'}))
    duration = forms.IntegerField(label='Max Duration', widget=forms.NumberInput(attrs={'class': 'font-weight-bold col-md-3','id': 'duration'}))
    timestep = forms.IntegerField(label='Time Step', widget=forms.NumberInput(attrs={'class': 'font-weight-bold col-md-3','id': 'timeStep', 'oninput': 'checkMultiple()'}))