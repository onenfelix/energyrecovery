function checkMultiple(x) {
	duration = document.getElementById('duration').value;
	timeStep = document.getElementById('timeStep').value;
	if (duration % timeStep == 0) {
		result = document.getElementById('multiple').innerHTML = timeStep + ' is a multiple of ' + duration;
		return result;
	} else {
		result = document.getElementById('multiple').innerHTML = timeStep + ' is not a multiple of ' + duration;
		return result;
	}
}
